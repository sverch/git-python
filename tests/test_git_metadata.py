# flake8: noqa
import pytest
from git_python import git_metadata


@pytest.fixture
def metadata(metadata_loader):
    return git_metadata.GitMetadata(metadata_loader=metadata_loader)


def test_get_local_branches(metadata):
    assert metadata.get_local_branches() == ["23007-replace-settings-gear-in-projects-etc-with-a-tab",
                                             "local-branch", "master"]

def test_get_remote_branches(metadata):
    assert metadata.get_remote_branches() == ["remotes/origin/1163-deleting-a-project-results-in-a-large-number-of-sql-requests-that-could-be-optimized",
                                              "remotes/origin/1163-deleting-a-project-results-in-a-large-number-of-sql-requests-that-could-be-optimized-paco",
                                              "remotes/origin/12818-build-status-favicon",
                                              "remotes/origin/12910-personal-snippet-prep-2-jej",
                                              "remotes/origin/13284-allow-uploading-of-multiple-files-via-web-interface",
                                              "remotes/origin/14192-issues-closed-by-merge-requests",
                                              "remotes/origin/14898-all-developers-can-push-to-protected-branch-by-default",
                                              "remotes/origin/15041-Add-Custom-CI-Config-Path",
                                              "remotes/origin/15457-fix-due-date-sort",
                                              "remotes/origin/master"]

def test_get_local_head(metadata):
    assert metadata.get_local_head() == "23007-replace-settings-gear-in-projects-etc-with-a-tab"

def test_get_remote_head(metadata):
    assert metadata.get_remote_head() == "origin/master"

def test_get_detached_local_head(metadata):
    metadata.metadata_loader.git_contents["HEAD"] = "detached_commit"
    assert metadata.get_local_head() == "detached_commit"
