"""
Stub class to test git config loader.
"""
# flake8: noqa
import pytest
from git_python import git_metadata_loader


# TODO: Test corrupt metadata cases, like a head pointing to a nonexistent ref
class StubGitMetadataLoader(git_metadata_loader.GitMetadataLoader):
    """Stub that pretends to be a git metadata dir."""
    def __init__(self):
        self.git_root = "/tmp/just-to-be-safe-fake-gitroot"
        self.git_contents = {
                "refs": {
                    "heads": {
                        "master": "commit_hash",
                        "local-branch": "commit_hash",
                        "23007-replace-settings-gear-in-projects-etc-with-a-tab": "commit_hash"
                    },
                    "remotes": {
                        "origin": {
                            "HEAD": "ref: refs/remotes/origin/master",
                            "master": "bd50b152412dc6c5f2959228110256bfd6b9e81d"
                        }
                    }
                },
                "HEAD": "ref: refs/heads/23007-replace-settings-gear-in-projects-etc-with-a-tab",
                "packed-refs": "# pack-refs with: peeled fully-peeled\n"
                               "a7fa7bc23813efedabd994f55fdc709066b1e9ab refs/remotes/origin/1163-deleting-a-project-results-in-a-large-number-of-sql-requests-that-could-be-optimized\n"
                               "75482edf4da265c1b4a94391e32e894450b93520 refs/remotes/origin/1163-deleting-a-project-results-in-a-large-number-of-sql-requests-that-could-be-optimized-paco\n"
                               "ed52ed6a58e7016b0c6120440a030c2c19f5fb6a refs/remotes/origin/12818-build-status-favicon\n"
                               "161a70fb3e2204d59ee79d17a162512e9738c680 refs/remotes/origin/12910-personal-snippet-prep-2-jej\n"
                               "9ea7131892f62cbab699fdd6d75f1afd792a611f refs/remotes/origin/13284-allow-uploading-of-multiple-files-via-web-interface\n"
                               "6f0e18d1a278b3f76e5ce62667d5fa0dd319ead0 refs/remotes/origin/14192-issues-closed-by-merge-requests\n"
                               "952ed20a786367c869cafec038b8d3174017b1bb refs/remotes/origin/14898-all-developers-can-push-to-protected-branch-by-default\n"
                               "35674fcd4732681286224c1c5fc92386ff53db7f refs/remotes/origin/15041-Add-Custom-CI-Config-Path\n"
                               "7ad7d3d3b0737927797a6eb86480ab3e147ababe refs/remotes/origin/15457-fix-due-date-sort\n"
            }

    def __resolve_path(self, metadata_path):
        contents = self.git_contents
        for component in metadata_path.split("/"):
            if component not in contents:
                raise RuntimeError("Invalid metadata_path in test suite")
            contents = contents[component]
        return contents

    def load(self, metadata_path):
        """
        Returns the git metadata at metadata_path under the .git dir.
        """
        return self.__resolve_path(metadata_path)

    def list(self, metadata_path):
        """
        Returns the git files under metadata_path under the .git dir.
        """
        return list(self.__resolve_path(metadata_path).keys())

    def get_git_root(self):
        """Returns the fake root of the git repository"""
        return self.git_root


@pytest.fixture
def metadata_loader():
    return StubGitMetadataLoader()
