import pytest
from click.testing import CliRunner
from git_python import cli


@pytest.fixture
def runner():
    return CliRunner()


def test_cli(runner):
    result = runner.invoke(cli.cli)
    assert result.exit_code == 0
    assert not result.exception
    assert "Usage" in result.output.strip()


# TODO: Find a better way to test this, since this assumes we're in a git repo
# when we run the tests.
def test_git_branch(runner):
    result = runner.invoke(cli.cli, ['branch'])
    assert not result.exception
    assert result.exit_code == 0


# TODO: Catch missing remote branch issue.
# def test_git_branch_dash_a(runner):
#     result = runner.invoke(cli.cli, ['branch', '-a'])
#     assert result.exit_code == 0
#     assert not result.exception
#     assert result.output.strip() == 'Called git branch -a'
