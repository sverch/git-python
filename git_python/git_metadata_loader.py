"""
Utility class that locates the .git dir and loads the raw metadata from it.
"""
import os
from contextlib import contextmanager


class GitMetadataLoader(object):
    """Class that loads the raw data from the .git metadata directory."""
    def __init__(self):
        pass

    # https://stackoverflow.com/a/37996581
    @contextmanager
    def __cwd(self, path):
        oldpwd = os.getcwd()
        os.chdir(path)
        try:
            yield
        finally:
            os.chdir(oldpwd)

    def __git_root(self):
        if os.path.exists(".git"):
            return os.getcwd()
        if os.getcwd() == "/":
            # TODO: Stop at filesystem boundary like git
            raise RuntimeError("Not a git repository (or any parent up to /)")
        with self.__cwd(".."):
            return self.__git_root()

    def load(self, metadata_path):
        """
        Returns the git metadata at metadata_path under the .git dir.
        """
        full_metadata_path = os.path.join(self.__git_root(), ".git", metadata_path)
        if os.path.exists(full_metadata_path):
            with open(full_metadata_path) as metadata_file:
                return metadata_file.read()
        else:
            return None

    def list(self, metadata_path):
        """
        Returns the list of files under metadata_path under the .git dir.
        """
        return os.listdir(os.path.join(self.__git_root(), ".git", metadata_path))

    def get_git_root(self):
        """Returns the root of the git repository"""
        return self.__git_root()
