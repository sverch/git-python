"""
Class that provides a high level interface to git metadata.
"""
import re
from git_python import git_metadata_loader
import subprocess

BRANCH_REF_REGEX = ".* refs/(remotes/.*)"
REMOTE_HEAD_REGEX = "ref: refs/remotes/(.*)"
LOCAL_HEAD_REGEX = "ref: refs/heads/(.*)"

class GitMetadata(object):
    """Class that provides a high level interface to git metadata."""
    def __init__(self,
                 metadata_loader=git_metadata_loader.GitMetadataLoader()):
        self.metadata_loader = metadata_loader

    def __get_packed_branches(self):
        """Get branches out of packed-refs"""
        branches = []
        packed_refs = self.metadata_loader.load("packed-refs")
        if not packed_refs:
            return []
        for ref in packed_refs.split("\n"):
            if ref.startswith("#"):
                continue
            branch_ref_match = re.search(BRANCH_REF_REGEX, ref)
            if branch_ref_match:
                branches.append(branch_ref_match.group(1))
        return branches

    def get_local_branches(self):
        """Get local branch information."""
        return sorted(self.metadata_loader.list("refs/heads"))

    def get_local_head(self):
        """Get local head information."""
        local_head = self.metadata_loader.load("HEAD")
        local_head_match = re.search(LOCAL_HEAD_REGEX, local_head)
        if local_head_match:
            return local_head_match.group(1)
        else:
            return local_head  # DETACHED HEAD

    # TODO: Handle multiple remotes
    def get_remote_branches(self):
        """Get remote branch information."""
        packed_branches = self.__get_packed_branches()
        remote_branches = self.metadata_loader.list("refs/remotes/origin")
        remote_branches = [rb for rb in remote_branches if rb != "HEAD"]
        remote_branches = ["remotes/origin/%s" % rb for rb in remote_branches]
        return sorted(packed_branches + remote_branches)

    # TODO: Handle multiple remotes
    def get_remote_head(self):
        """Get remote head information."""
        remote_head = self.metadata_loader.load("refs/remotes/origin/HEAD")
        if not remote_head:
            return None
        remote_head_match = re.search(REMOTE_HEAD_REGEX, remote_head)
        return remote_head_match.group(1)

    def __run_cmd(self, cmd):
        try:
            return subprocess.check_output(" ".join(cmd), stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError, ex:
            print "--------error------"
            print ex.cmd
            print ex.message
            print ex.returncode
            print ex.output

    def get_commit_numbers_by_author(self, startref):
        """
        Get commits numbers by author.

        git shortlog -n -s
        """
        raw_rev_list = self.__run_cmd(["git", "rev-list", "HEAD", "--pretty=tformat:\"%an\""])
        lines = raw_rev_list.split("\n")
        authors = [author for _, author in zip(lines[::2], lines[1::2])]
        commit_counts = {}
        for author in authors:
            if author not in commit_counts:
                commit_counts[author] = 1
            else:
                commit_counts[author] = commit_counts[author] + 1
        return commit_counts
