import click
from git_python import git_metadata
from termcolor import colored


@click.command()
@click.option('--all', '-a', is_flag=True, help='List all branches.')
def branch(all=False):
    local_head = git_metadata.GitMetadata().get_local_head()
    local_branches = git_metadata.GitMetadata().get_local_branches()

    if local_head not in local_branches:
        detached_head_output = "(HEAD detached at %s)" % local_head[0:7]
        click.echo("* %s" % colored(detached_head_output, 'green'))

    for local_branch in local_branches:
        if local_head == local_branch:
            click.echo("* %s" % colored(local_branch, 'green'))
        else:
            click.echo("  %s" % local_branch)

    if all:
        remote_head = git_metadata.GitMetadata().get_remote_head()
        remote_branches = git_metadata.GitMetadata().get_remote_branches()
        remote_branches = [colored(rb, 'red') for rb in remote_branches]
        # TODO: Resolve detached head to a tag if needed
        if remote_head:
            remote_branches.append("%s -> %s" % (colored("remotes/origin/HEAD",
                                                         'red'), remote_head))
        remote_branches = ["  %s" % rb for rb in remote_branches]
        remote_branches.sort()
        click.echo("\n".join(remote_branches))
