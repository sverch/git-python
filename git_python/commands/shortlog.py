import click
from git_python import git_metadata
from termcolor import colored


@click.command()
@click.option('--numbered', '-n', is_flag=True,
              help='Sort output according to the number of commits per author instead of author alphabetic order. (currently always set)')
@click.option('--summary', '-s', is_flag=True,
              help='Suppress commit description and provide a commit count summary only. (currently always set)')
def shortlog(summary=True, numbered=True):
    # TODO: Pass ref
    commits_per_author = git_metadata.GitMetadata().get_commit_numbers_by_author("HEAD")
    # Make to tuples so we can sort
    commits_per_author_tuples = sorted(list(commits_per_author.iteritems()), key=lambda x: x[1], reverse=True)
    # For output formatting
    max_commit_num = max(commits_per_author_tuples, key=lambda x: x[1])[1]
    for author, commits in commits_per_author_tuples:
        click.echo(" %*s  %s" % (len(str(max_commit_num)), commits, author))
