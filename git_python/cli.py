import click
from git_python.commands import branch
from git_python.commands import shortlog


@click.group()
def cli():
    pass


cli.add_command(branch.branch)
cli.add_command(shortlog.shortlog)
