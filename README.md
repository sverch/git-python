# Git Python

Python experiments with git

# Building

Run Tests:

    $ tox

Create a distributable pex package with no python dependency:

    $ tox -e py27-package
    $ dist/git --help

# Usage

To use it:

    $ git --help
